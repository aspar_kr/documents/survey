# Summary of interviews

## Development tips from experts

### Implementation language

From David Meijer [^Meijer-interview] and Johanna Rhodes [^Johanna-Rhodes-interview] I got tips on how to construct a database sharing system: I should work in small increments and deliver a small workable version quickly so I can get feedback from the community. I can make a database with python `flask` but `rust`  should also work. It is possible to run the server on the `bioinformatics.wur.nl` address. It would be possible to make the database using rust, but it has not yet been done before.[^Meijer-interview][^Jasper-interview] 

### Use of ontology

The use of the [MIXS ontology](https://bioportal.bioontology.org/ontologies/MIXS) would fit the database.[^Jasper-interview]

## Community needs from researchers

While researchers in the field of *A. fumigatus* research do not have much programming knowledge, some know `R`; it will be useful to offer (video) guides on programmatic access to the `ASPAR_KR` database via `R`, of course, it should be possible to query from a web interface and download the `csv`.[^Martin-interview][^Riin][^Johanna-Rhodes-interview]  Both fundamental researchers, and researchers looking at the environmental distribution are conducting resistance assays [^Riin][^Martin-interview]

### Fundamental researchers

A researcher interested in fundamental biology of *Aspargillus fumigatus* might just use the resistance database part of `ASPAR_KR`, and put less emphasis of the genotype, because it is already defined. In fundamental research, there may also be an added value to unpublished information: some publications have results that do not fit the final paper, but might still be worth adding to the database.

### Environmental / epidemiological researchers

A researcher interested in the environmental distribution will use the environmental sample annotations. 

#### Additional requests

It would be interesting to link data of azole usage to the database.[^Johanna-Rhodes-interview] 

---

[^Meijer-interview]: Interview with David Meijer: https://git.wur.nl/aspar_kr/surveys/-/blob/main/survey_data/open_questions/people/David_Meijer.md
[^Johanna-Rhodes-interview]: Interview with Johanna Rhodes: https://git.wur.nl/aspar_kr/surveys/-/blob/main/survey_data/open_questions/people/Johanna_Rhodes.md
[^Jasper-interview]: Interview with Jasper Koehorst: https://git.wur.nl/aspar_kr/surveys/-/blob/main/survey_data/open_questions/people/Jasper_Koehorst.md
[^Riin]: Interview with Riin Muilu-Mäkelä: https://git.wur.nl/aspar_kr/surveys/-/blob/main/survey_data/open_questions/people/Riin_Muilu-M%C3%A4kel%C3%A4.md#fnref-1-4110
[^Martin-interview]: Interview with Martin Weichert: https://git.wur.nl/aspar_kr/surveys/-/blob/main/survey_data/open_questions/people/Martin_Weichert.md